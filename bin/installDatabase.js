#! /usr/bin/env node

//Initial admin account
const ADMIN_LOGIN="admin";

//See main/server/controller.js
const HASH_SALT_ROUNDS= 3;

//Required libraries
const serverConfig =require('../server.config.production.json');
const bcrypt = require('bcryptjs');
const generator = require('../main/server/passwordHelper');

//Database connection
var mongoose = require('mongoose');
mongoose.connect(serverConfig.MONGO_URL);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'Error connection to MongoDB:'));

//Create random password
var password = generator.generate({length: 20, numbers: true});
console.log("**********************************************************");
console.log("Initial Password for user "+ADMIN_LOGIN);
console.log(password);
console.log("**********************************************************");

//Add admin account
var User = require('../main/server/modelUser');
User.findOne({login:ADMIN_LOGIN}, function(err, adminUser) {
	if (err) console.log(err);
	if (adminUser) {
		console.log('Admin account already exists - skipping');
		mongoose.connection.close();
	}
	else {
		adminUser=new User({
			name: "Administrator",
			login: ADMIN_LOGIN,
			password: bcrypt.hashSync(password, HASH_SALT_ROUNDS),
			email: "",
			roles: ['admin','abcd','survey','ideas'],
			active:true });
		adminUser.save(function(err) {
			if (err) throw err;
			else console.log("Initial account and role created successfully");
			mongoose.connection.close();
		});
	}
})
