/*********************************************************
 * Configure app - change according to your setup
 *********************************************************/

//modules to import, also add to array allModules below!
import modAbcd from './modules/abcd/config';
import modAbout from './modules/about/config';
import modIdeas from './modules/ideas/config';
import modSurvey from './modules/survey/config';
import modTex from './modules/tex/config';
import modProjector from './modules/projector/config';

export default {
	author:'Lerntools-Team',	//author name
	pageTitle:'Lerntools',		//html head page title
	urlBackend:'/',				//backend api, leave as '/' when running your own backend
	urlImprint:'',				//url of external imprint page
	urlTerms:'',				//url of external terms page
	urlPrivacy:'',				//url of external privacy page
	urlHome:'',					//url of external link from home icon
	defaultLocale:'de',			//default locale
	fixNavbar: true,			//always display top navbar at fixed position
	allModules: [
		modAbout,
		modAbcd,
		modIdeas,
		modSurvey,
		modTex,
		modProjector
	]
}
