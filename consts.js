//Character constants
const CHARS_BASE=" .,;:!?/()={}'<>$€§%&#*\\[\\]\"+-";
const CHARS_EXTENSION="¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ→↓←·…";
const CHARS_GREEK="ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρςστυφχψωϑ";
const CHARS_EMOTE="😀😁😂😃😄😅😆😇😈😉😊😕😔😓😒😑😐😏😎😍😌😋😖😗😘😙😚😛😜😝😞😟😠😫😪😩😨😧😦😥😣😢😡😭😮😯😰😱😲😳😴😵😶🙀😿😾😽😼😻😺😹😸😷🙃👍👎";
const CHARS_ELSE="✅✓✔✕✖❌✗✘„“–";
const CHARS_CONTROL="\n\t\r";
const ALLOWED_CHARS="[\\w "+CHARS_BASE+CHARS_GREEK+CHARS_EXTENSION+CHARS_EMOTE+CHARS_CONTROL+CHARS_ELSE+"]";
const ALLOWED_CHARS_AREA="[\\w "+CHARS_BASE+CHARS_GREEK+CHARS_EXTENSION+CHARS_EMOTE+CHARS_CONTROL+CHARS_ELSE+"]";
//const CHARS_SPECIAL="\n\r\t";
//Emoticons
exports.CHARS_EMOTE=CHARS_EMOTE;

//Regular expressions for input validation in text fields
exports.REGEX_COLOR = new RegExp("^#[a-fA-F0-9]{6,8}$");
exports.REGEX_ROLE = new RegExp("^[a-zA-Z]{1,20}$");
exports.REGEX_LOGIN = new RegExp("^[a-zA-Z0-9]{1,100}$");
exports.REGEX_ALPHANUM = new RegExp("^[a-zA-Z0-9]*$");
exports.REGEX_NUMBER = new RegExp("^[0-9]+$");
exports.REGEX_TEXT = new RegExp("^"+ALLOWED_CHARS+"*$");
exports.REGEX_TEXT_30 = new RegExp("^"+ALLOWED_CHARS+"{1,30}$");
exports.REGEX_TEXT_50 = new RegExp("^"+ALLOWED_CHARS+"{1,50}$");
exports.REGEX_TEXT_100 = new RegExp("^"+ALLOWED_CHARS+"{1,100}$");
exports.REGEX_TEXT_200 = new RegExp("^"+ALLOWED_CHARS+"{1,200}$");
exports.REGEX_TEXT_300 = new RegExp("^"+ALLOWED_CHARS+"{1,300}$");

//Regular expressions for input validation in text areas
exports.REGEX_TEXT_AREA = new RegExp("^"+ALLOWED_CHARS_AREA+"*$");
exports.REGEX_TEXT_AREA_300 = new RegExp("^"+ALLOWED_CHARS_AREA+"{1,300}$");
exports.REGEX_TEXT_AREA_500 = new RegExp("^"+ALLOWED_CHARS_AREA+"{1,500}$");
exports.REGEX_TEXT_AREA_1000 = new RegExp("^"+ALLOWED_CHARS_AREA+"{1,1000}$");
exports.REGEX_TEXT_AREA_2000 = new RegExp("^"+ALLOWED_CHARS_AREA+"{1,2000}$");
exports.REGEX_TEXT_AREA_5000 = new RegExp("^"+ALLOWED_CHARS_AREA+"{1,5000}$");

//Regular expressions for detection of media types
exports.REGEX_IMAGE =  /\.(png|jpe?g|gif|svg|bmp)$/i
exports.REGEX_AUDIO = /\.(mp3|wav|ogg|mpga)$/i
exports.REGEX_VIDEO = /\.(mpe?g|mkv|mp4|avi|mov|mpv|wmf|webm)$/i

//Other regular expressions
exports.REGEX_TICKET = RegExp("^[a-zA-Z0-9]{6,100}$");
exports.REGEX_LOGIN = RegExp("^[a-zA-Z0-9]{3,100}$");
exports.REGEX_PASSWORD = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})");
exports.REGEX_DATA_URI = new RegExp("^data:.+\/(.+);base64,(.*)$");
exports.REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
exports.REGEX_LANGUAGE = new RegExp("^[a-zA-Z]{2}$")
exports.REGEX_URL = /^(?:http(s)?:\/\/)?[\wäöü.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.+REGEX_PATH]+$/gm
exports.REGEX_PATH = new RegExp("^[a-zA-Z0-9ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ /._-]{1,1000}$")
exports.REGEX_NICKNAME = new RegExp("^"+ALLOWED_CHARS+"{3,50}$");
exports.REGEX_USERNAME = new RegExp("^"+ALLOWED_CHARS+"{3,100}$");
exports.REGEX_COMMENT_300 = new RegExp("^"+ALLOWED_CHARS_AREA+"{0,300}$");
exports.REGEX_COMMENT_1000 = new RegExp("^"+ALLOWED_CHARS_AREA+"{0,1000}$");
exports.REGEX_UPLOAD_ID = new RegExp("^[a-zA-Z0-9]{15,128}[.][a-zA-Z0-9]{1,10}$");

//Escape special HTML characters
exports.HTML_ENTITY_MAP = {
	'&': '&amp;',
	'<': '&lt;',
	'>': '&gt;',
	'"': '&quot;',
	"'": '&#39;',
	'/': '&#x2F;',
	'`': '&#x60;',
	'=': '&#x3D;'
};

//Supported languages
exports.SUPPORTED_LANGUAGES=['de', 'en']

//Maximum Size for inline images
exports.MAX_INLINE_IMAGE_SIZE=.5*1024*1024;

//Maximum Size for uploaded files
exports.MAX_UPLOAD_SIZE=5*1024*1024;
exports.UPLOAD_MIME_TYPES=[
	"image/png","image/jpeg","image/svg","image/svg+xml",
	"audio/mpeg","audio/ogg",
	"video/mp4","video/webm",
	"application/pdf"
];

//Allowed characters for captchas
exports.CAPTCHA_CHARS="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
