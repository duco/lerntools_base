import store from './store.js';

const colors={
	dark: ['transparent','#0070E0','#00e070','#687480','white'],
	light: ['transparent','#0060a0','#00a060','#687480','black']
};

/*
provides functions for accessing theme-colors (arrays dark, light) as well as a function to autmatically adjust the backgroundColor of elements owning the css class bg-Var
*/
export default {
	colors:colors,
	paint:colors[store.design],
	componentToHex: function(c) {
		var hex= c.toString(16);
		return hex.length== 1 ? "0" + hex : hex;
	},
	rgbToHex: function(r, g, b) {
		return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
	},
	randomColor: function() {
		var r=Math.floor(150*Math.random());
		var b=Math.floor(150*Math.random());
		var g=Math.floor(150*Math.random());
		return this.rgbToHex(r,g,b);
	},
	setBgVar: function(parent) {
		if (!parent) parent=document.body;
		var elements=parent.getElementsByClassName('bg-var');
		var total=elements.length;
		if (total==1) elements[0].style.backgroundColor=this.rgbToHex(0, 96, 160);
		else if (total>1){
			for (var num=0; num<total; num++) {
				var mod=Math.round(64*num/(total-1));
				elements[num].style.backgroundColor=this.rgbToHex(0, 96+mod, 160-mod);
			}
		}

	}
}
