import MarkdownIt from 'markdown-it';
import MarkdownItTexmath from 'markdown-it-texmath';

//init markdown
var markdownIt=new MarkdownIt({breaks:true});
markdownIt.use(MarkdownItTexmath);

export default {
	/*
	convert markdown string to html using inline mode → result is not wrapped into an <p> paragraph
	line breaks are converted to <br>
	*/
	render: function(md) {
		var html=markdownIt.renderInline(md);
		//return html.substring(3,html.length-6)
		return html;
	}
}
