const consts=require('../../consts.js');

//Escape dangerous characters in html
exports.escapeHTML=function(string) {
	var map=consts.HTML_ENTITY_MAP;
	return String(string).replace(/[&<>"'`=\/]/g, function (s) {
		return map[s];
	});
}
