//External libs
const { check,body,param,validationResult }=require('express-validator/check');
const { sanitize,sanitizeBody }=require('express-validator/filter');
const bcrypt=require('bcryptjs');
const debug=require('debug')('main');
const generator=require('./passwordHelper');
const jwt=require('jsonwebtoken');
const path=require('path');
const svgCaptcha=require('svg-captcha');
const cron=require('node-cron');
const requestIp=require('request-ip');

//Internal libs
const consts=require ('../../consts.js');
const config=__config;
const sendInfoMail=require('./smtpHelper').sendInfoMail;
const escapeHTML=require('./htmlHelper').escapeHTML;

//Database schemas
var User=require('./modelUser');
var Captcha=require('./modelCaptcha');
var Reset=require('./modelReset');
var Register=require('./modelRegister');
var RefreshToken=require('./modelRefreshToken');
var BruteForceFilter=require('./modelBruteForceFilter');
var Tan=require('./modelTan.js');
const ShortLink=require('./modelShortLink.js');

//Password hashing
const HASH_SALT_ROUNDS=3;
const SHORTLINK_TICKET_LENGTH=8;
const SHORTLINK_LIFETIME=365;	//minimum lifetime of shortlinks in days

/***********************************************************
	Information on Software Version and Packages
***********************************************************/

/*
Get Info on configured packages reading package.json, only for admin
*/
exports.queryPackages=function(req,res,next) {
	var info=require('../../package.json');
	return res.json({info:info});
}

/*
Get Info on version and Name, only for authenticated users
*/
exports.queryInfo=function(req,res,next) {
	var info=require('../../package.json');
	return res.json({info: {name:info.name, version:info.version}})
}


/***********************************************************
	Login & Logout & Tokens
***********************************************************/

/*
 login
 */
exports.doLogin=[
	body('login').isLength({ min: 1 }),
	body('password').isLength({ min: 1 }),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var login=req.body.login;
		var password=req.body.password;
		//check for brute force attack
		BruteForceFilter.findOne({ login: login }).exec(function (err, bffEntry) {
			if (err) debug(err);
			if (!bffEntry) bffEntry=new BruteForceFilter({login:login});
			var delayTime=bffEntry.blockedUntilTs - Date.now();
			delayTime=delayTime>0 ? Math.round(delayTime) : 0;
			bffEntry.numberFailedLogins=Math.min(10,bffEntry.numberFailedLogins+1);
			bffEntry.blockedUntilTs=Date.now()+1000*3*bffEntry.numberFailedLogins;
			debug('Delaying login of '+login+' by '+delayTime+' ms');
			//delay response
			setTimeout( () => {
				//check data
				User.findOne({$or: [{ login: login },{email:login}]}).exec(function (err, user) {
					if (err) {debug(err); return res.json([{msg:'login-failed'}]);}
					//user not found
					if (!user) {
						debug('Login not found for '+login);
						bffEntry.save((err)=> { if (err) debug(err); })
						return res.json([{msg:'login-failed'}]);
					}
					//user locked
					if (!user.active) return res.json([{msg:'account-locked'}]);
					//password ok
					if (bcrypt.compareSync(password, user.password)) {
						debug('Successfull login for '+login);
						var accessToken=generateAccesToken(req,login);
						var refreshToken=new RefreshToken({
							login:user.login,
							token:generator.generate({length: 64, numbers: true}),
							ip:requestIp.getClientIp(req),
							expires:new Date(Date.now()+1000*config.JWT.REFRESH_EXPIRES)
						});
						user.lastLogin=Date.now();
						user.expireInfos=0;
						user.save(function(err) {
							if (err) return res.json([{msg:'unknown-error'}]);
							return res.json({
								login:user.login,
								name:user.name,
								roles:user.roles,
								lang:user.lang,
								email:user.email,
								token: {
									token_type:"bearer",
								    access_token:accessToken,
								    expires_in:config.JWT.ACCESS_EXPIRES,
								    refresh_token:refreshToken.token
								}
							});
						});

						//Save RefreshToken to DB
						refreshToken.save(function(err, token) {
							if (err) debug(err);
						})
						//Clear BruteForceFilter for user
						BruteForceFilter.deleteOne({login:login}).exec(function(err) {
							if (err) debug(err);
						})
					}
					//password wrong
					else {
						debug('Login failed for '+login);
						bffEntry.save((err)=> { if (err) debug(err); });
						return res.json([{msg:'login-failed'}]);
					}
				})
			}, delayTime);
		});
	}
]

/*
 Logout
 */
exports.doLogout=function(req, res, next) {
	if (!res.locals.user) return res.json({});
	//Invalidate RefreshToken for ALL sessions of this user (more secure)
	var user=res.locals.user;
	RefreshToken.deleteMany({'login':user.login}, function(err) {
		if (err) debug(err);
		else debug('Deleted all refreshTokens of '+user.login);
		res.json({});
	})
}

/*
 Refresh Token
 */
exports.newAccessToken=[
	body('login').matches(consts.REGEX_USERNAME),
	body('token').matches(consts.REGEX_PASSWORD),
	function(req,res,next ) {
		if (!validationResult(req).isEmpty()) return res.json({});
		var login=req.body.login;
		var token=req.body.token;
		//look up in database
		RefreshToken.findOne({login:login, token: token, expires: {$gte: new Date() }}, (err, refreshToken )=> {
			if (err) debug(err);
			if (!refreshToken)  {
				debug('RefreshToken expired for '+login);
				return res.json({});
			}
			//check ip if configured
			var ip=config.JWT.RESTRICT_IP ? requestIp.getClientIp(req) : '';
			if (ip && refreshToken.ip !=ip) return res.json({});
			//generate new token
			var accessToken=generateAccesToken(req,login);
			debug('Token Refresh for '+login);
			return res.json({ accessToken:accessToken });
		})
	}
]

/*
 Generate valid access token for specific user
 Use secret and ip if configured
 */
function generateAccesToken(req,login) {
	var ip=config.JWT.RESTRICT_IP ? requestIp.getClientIp(req) : '';
	var now=Math.floor(Date.now() / 1000);
	var accessToken=jwt.sign(
		{ login: login, iat: now },
		ip+config.JWT.SECRET,
		{ expiresIn: config.JWT.ACCESS_EXPIRES }
	);
	return accessToken;
}

/***********************************************************
	Manage TANs for critical actions
***********************************************************/

exports.startTanProcess=function(req,res,next) {
	var user=res.locals.user;
	if (!user) return res.json({});
	var code=generator.generate({length: 6, numbers: true});
	var expires=new Date(Date.now()+1000*config.TAN_EXPIRES)
	var tan=new Tan({login:user.login, code:code, expires:expires});
	tan.save(function(err) { if (err) debug(err);});
	var title="Transaction Code";
	var html="<p>Dear "+escapeHTML(user.name)+",<br><br>The code to confirm your action is: "+code+"</p><p>If you have not requested a code, please ignore this message.</p>";
	//don't send tan → for development only
	if (config.TAN_IGNORE) {
		debug("Transaction Code: "+tan.code);
	}
	else {
		if (user.email) sendInfoMail(user.email,title,html);
		else sendInfoMail(config.USER_ADMIN_MAIL,title,html);	// allow new admin to set email address*/
	}
	res.json({});
}

/* Check, if request contains vaild tan for user*/
exports.checkTan=[
	check('tan').matches(consts.REGEX_TICKET),
	(req,res,next)=> {
		//return if tan validation fails or request is missing authenticated user
		if (!validationResult(req).isEmpty() || !res.locals.user) return res.json([{msg:'invalid-tan'}]);
		//accept all tans when configured → for development only
		if (config.TAN_IGNORE) return next();
		//search and check tan
		var tan=req.body.tan || req.query.tan;
		var login=res.locals.user.login;
		Tan.findOne({login:login, code: tan, expires: {$gte: new Date() }}, (err, tan )=> {
			if (err) debug(err);
			if (!tan) return res.json([{msg:'invalid-tan'}]);
			//success
			tan.delete();
			debug("Valid TAN for "+res.locals.user.login)
			next();
		})
	}
]

/***********************************************************
	User profile
***********************************************************/


exports.myProfile=function(req,res,next) {
	var user=res.locals.user;
	if (!user) return res.json([{msg:'please-login'}]);
	return res.json({login:user.login, name:user.name, roles:user.roles, lang:user.lang, email:user.email });
}

/*
 Update user settings
 */
exports.updateUser=[
	body('name').matches(consts.REGEX_USERNAME),
	body('email').matches(consts.REGEX_EMAIL),
	body('lang').matches(consts.REGEX_LANGUAGE),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var user=res.locals.user;
		if (!user) return res.json([{msg:'please-login'}]);
		//Update DB
	    User.findOne({ login: user.login }).exec(function (err, dbUser) {
			dbUser.name=req.body.name;
			dbUser.email=req.body.email;
			dbUser.lang=req.body.lang;
			dbUser.save(function(err) {
				if (err) return res.json([{msg:'database-error'}]);
				res.json({name:dbUser.name,lang:dbUser.lang,email:dbUser.email,login:dbUser.login});
			})
		})
	}
]

/*
 Change password from profile
 */
exports.updatePassword=[
	body('password').matches(consts.REGEX_TEXT),
	body('passwordNew').matches(consts.REGEX_PASSWORD),
	body('passwordConfirm').isLength({ min: 1 }),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var user=res.locals.user;
		if (!user) return res.json([{msg:'please-login'}]);
		//load form data
		var password=req.body.password;
		var passwordNew=req.body.passwordNew;
		var passwordConfirm=req.body.passwordConfirm;
		//load user from db
    	User.findOne({ login: user.login }).exec(function (err, dbUser) {
			//check old password
			if (!bcrypt.compareSync(password, dbUser.password)) return res.json([{msg:'wrong-password'}]);
			//compare passwords
			if (passwordNew!=passwordConfirm) return res.json([{msg:'passwords-dont-match'}]);
			//update password
			dbUser.password=bcrypt.hashSync(passwordNew,HASH_SALT_ROUNDS);
			dbUser.save(function(err) {
				if (err) return res.json([{msg:'database-error'}]);
				res.json({});
			})
		});
	}
]

exports.deleteOwnUser=[
	body('password').isLength({ min: 8 }),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var user=res.locals.user;
		if (!user) return res.json([{msg:'please-login'}]);
		//load form data
		var password=req.body.password;
		//load user from db
    	User.findOne({ login: user.login }).exec(function (err, dbUser) {
			//check old password
			if (!bcrypt.compareSync(password, dbUser.password)) return res.json([{msg:'wrong-password'}]);
			deleteUserAndData(user.login);
			return res.json({});
		});
	}
]


/***********************************************************
	User administration
************************************************************/

/*
Manage Users
*/
exports.getUserList=function(req, res, next) {
	//Collect information on users
	User.find({},'name login lastLogin active comment roles lang email').sort({ name: 'asc'}).exec(function (err, userList) {
		 if (err) return res.json([{msg:'database-error'}]);
		 return res.json({userList:userList});
	 });
}

/*
Add user
*/
exports.addUser=[
	body('login').matches(consts.REGEX_USERNAME),
	body('password').matches(consts.REGEX_PASSWORD),
	body('email').matches(consts.REGEX_EMAIL),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var login=req.body.login;
		var password=req.body.password;
		var email=req.body.email;
		//Account alread existing?
		User.findOne({ $or : [{login: login},{email:email}] }).exec(function (err, user) {
			if (err) {
				debug(err);
				return res.json([{msg:'database-error'}]);
			}
			if (user) return res.json([{msg:'username-taken'}]);
			//Add new User
			var hash=bcrypt.hashSync(password, 3);
			var newUser=new User({login: login, email:email, password: hash, active:true, name:login, comment:'created '+new Date()});
			newUser.save(function(err) {
				if (err) {
					debug(err);
					return res.json([{msg:'database-error'}]);
				}
				next();
			});
		});
	}
]

/*
Save user
*/
exports.saveUser=	[
	body('login').matches(consts.REGEX_LOGIN),
	body('name').matches(consts.REGEX_USERNAME),
	body('email').matches(consts.REGEX_EMAIL),
	body('lang').matches(consts.REGEX_LANGUAGE),
	sanitizeBody('password'),
	body('comment').matches(consts.REGEX_COMMENT_300),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		res.locals.page=req.body;
		//Load data
		var login=req.body.login;
		var name=req.body.name;
		var email=req.body.email.toLowerCase();
		var lang=req.body.lang;
		var password=req.body.password;
		var comment=req.body.comment;
		var active=req.body.active;
		var roles=[];
		if (!req.body.roles) return res.json([{msg:'forbidden'}]);
		for (role of req.body.roles ) {
			if (role && role.match(consts.REGEX_ROLE))  roles.push(role);
		}
		//Update user
		User.findOne({ login: login }).exec(function (err, user) {
			if (err || !user) return res.json([{msg:'database-error'}]);
			user.name=name,
			user.login=login;
			user.email=email;
			user.comment=comment;
			user.roles=roles;
			user.active=active;
			user.lang=lang;
			if (password && password.length>0) user.password=bcrypt.hashSync(password,HASH_SALT_ROUNDS);
			user.save(function(err) {
				if (err) return res.json([{msg:'database-error'}]);
				next();
			});
		});
	}
]

/*
Delete user
- Authenticate via checkAuth OR checkDeleteUser
*/
exports.deleteUser=[
	param('login').matches(consts.REGEX_USERNAME),
	async function(req, res, next) {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var login=req.params.login;
		console.log(login);
		if (res.locals.user.roles.indexOf('admin')>-1 && login==res.locals.user.login) return res.json([{msg:'cant-delete-yourself'}]);
		await deleteUserAndData(login);
		next();
	}
]

/*
Send message to all registered users
- Authenticate via checkAuth
*/
exports.sendMessage=[
	body('subject').matches(consts.REGEX_TEXT_100),
	body('message').matches(consts.REGEX_TEXT_AREA),
   (req, res, next)=> {
	   if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
	   var message=req.body.message;
	   var subject=req.body.subject;
	   User.find({}, function (err, userList) {
		   if (err) return res.json([{msg:'database-error'}]);
		   var html=message.replace(/\r?\n/g, '<br>');
		   for (user of userList) {
			  sendInfoMail(user.login,subject,html);
		   }
		   res.json({});
	   });
   }
]

/*
 Delete Refresh Token for specific user
 */
exports.deleteRefreshToken=[
	param('login').matches(consts.REGEX_USERNAME),
	function(req,res,next ) {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var login=req.params.login;
		//look up in database
		RefreshToken.deleteMany({login:login}, (err)=> {
			if (err) debug(err);
		})
	}
]


/*
 Check for unused accounts and send info / delete, every sunday at 11:55
 */
cron.schedule("55 11 * * 0", cleanAccounts);
function cleanAccounts() {
	if (!config.ACCOUNT_CLEANUP || !config.ACCOUNT_CLEANUP.ENABLED || !config.ACCOUNT_CLEANUP.EXPIRE_PERIOD || !config.ACCOUNT_CLEANUP.GRACE_WEEKS) return;
	debug('Cleaning old accounts which have not been used for a given period / or never been used');
	var expireDate=new Date(Date.now() - 1000*3600*24*config.ACCOUNT_CLEANUP.EXPIRE_PERIOD);	//x days back
	User.find( {$or: [{ lastLogin: {$lte: expireDate}}, {lastLogin: null}]}, function(err, userList) {
		if (err) { debug(err); return}
		for (var user of userList) {
			if (user.expireInfos<config.ACCOUNT_CLEANUP.GRACE_WEEKS) {
				user.expireInfos++;
				user.save();
				debug('Sending expire info to '+user.login);
				//send mail
				var title="Account will expire shortly";
				var html="<p>Dear "+escapeHTML(user.name)+",<br><br>your account will be deleted shortly for either not having logged in for more than  "+config.ACCOUNT_CLEANUP.EXPIRE_PERIOD+" days or for never having logged in at all. Please log in soon.";
				sendInfoMail(user.email,title,html);
			}
			else {
				debug('Deleting '+user.login);
				//info to user
				var title="Account deleted";
				var html="<p>Dear "+escapeHTML(user.name)+",<br><br>your account and all associated data have been deleted. This process cannot be undone - but you can request a new account at any time.</p>";
				sendInfoMail(user.email,title,html);
				//info to admin
				title="Account deleted";
				html="<p>Account has been deletetd due to inactivity on "+config.URL_HOST+"</p>";
				html+="<p>Name: "+escapeHTML(user.name)+"</p>";
				html+="<p>Email: "+escapeHTML(user.email)+"</p>";
				html+="<p>Last login: "+user.lastLogin+"</p>";
				sendInfoMail(config.USER_ADMIN_MAIL,title,html);
				//delete
				deleteUserAndData(user.login);
			}
		}
	});
}

//delete user and all correlated data
async function deleteUserAndData(login) {
	//Delete user data

	for (var moduleName of __modules) {
		var controllerFile=path.join('..','..','/modules/',moduleName,'server','controller.js');
		var moduleController=require(controllerFile);
		//ORIG: if (moduleController.deleteUserData) {
		try {

		 if (moduleController.deleteUserData) {
			await moduleController.deleteUserData(login);
			console.log("Lösche endlich");
			debug('deleting all data for user '+login+' in Module '+moduleName);
		}
		}
		catch(err){
			debug('an error occured:'+ err);
		}

	}
	//Delete user account
	User.deleteOne({login:login}, function (err) {
		if (err) return res.json([{msg:'database-error'}]);
		debug('User '+login+' successfully deleted');
		return 'User '+login+' successfully deleted';
	});
}

/***********************************************************
	Self-Registration and Password-Reset
***********************************************************/

/*
Allow registration if configured in server config and request originates from same machine
*/
exports.queryRegistrationState=function(req,res,next) {
	var allowed=config.REGISTRATION.ALLOWED;
	res.json({allowed:allowed});
}

/*
Allow info mails if configured in server config
*/
exports.queryInfoMailState=function(req,res,next) {
	var allowed=config.INFO_MAIL_APPS;
	res.json({infoMailApps:allowed});
}

/*
 Create Captcha
 */
exports.createCaptcha=function(req, res, next) {
	var captcha=svgCaptcha.create({color:true, size:6, noise:3, charPreset:consts.CAPTCHA_CHARS});
	var ticket=generator.generate({length: 20, numbers: true});
	var dbEntry=new Captcha({text:captcha.text, ticket:ticket, created: new Date()});
	dbEntry.save(function(err) {
		if (err) {
			debug(err);
			return res.json([{msg:'unknown-error'}]);
		}
		return res.json({ticket:ticket, svg:captcha.data});
	});
}

/*
 Check Captcha, used in routing
 */
exports.checkCaptcha=[
	check('captchaTicket').matches(consts.REGEX_TEXT_50),
	check('captchaText').matches(consts.REGEX_TEXT_50),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var ticket=req.param.captchaTicket || req.body.captchaTicket;
		var text=req.param.captchaText || req.body.captchaText;
		Captcha.findOne({ticket:ticket, text:text}).exec(function (err, dbCaptcha) {
			if (!dbCaptcha) return res.json([{msg:'captcha-wrong'}]);
			dbCaptcha.delete();
			next();
		})
	}
]

/*
 Start Password reset process - send mail
*/
exports.startResetProcess=[
	body('email').matches(consts.REGEX_EMAIL),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//load form data
		var email=req.body.email.toLowerCase();
		//search email address
    	User.findOne({ email: email }, function (err, dbUser) {
			if (!dbUser) {
				debug('Request for non existing email '+email);
				//return res.json([{msg:'email-not-existing'}]);
				return res.json({}); //don't give hint on existing email Adresses
			}
			//create reset process
			var ticket=generator.generate({length: 20, numbers: true});
			var reset=new Reset({email:email, ticket:ticket, created: new Date()});
			reset.save(function(err) {
				if (err) {
					debug(err);
					return res.json([{msg:'unknown-error'}]);
				}
				//send mail
				var url=config.URL_PROTO+path.join(config.URL_HOST,config.URL_PREFIX,'app','#',"main-reset-confirm",encodeURIComponent(email),ticket);
				var title="Password Reset";
				var html="<p>A password reset was initiated for your account on page "+config.URL_HOST+"</p>";
				html+="<p>Please click the following link to complete the process:</p>";
				html+="<p><a href='"+url+"'>"+url+"</a></p>";
				html+="<p>If you did not start this request yourself, please ignore this email.</p>";
				sendInfoMail(email,title,html);
				return res.json({});
			})
		});
	}
]

/*
 Complete password reset process
 */
exports.completeResetProcess=[
	body('email').matches(consts.REGEX_EMAIL),
	body('passwordNew').matches(consts.REGEX_PASSWORD),
	body('passwordConfirm').isLength({ min: 1 }),
	body('ticket').matches(consts.REGEX_TICKET),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//load form data
		var email=req.body.email.toLowerCase();
		var ticket=req.body.ticket;
		var passwordNew=req.body.passwordNew;
		//compare passwords
		var passwordConfirm=req.body.passwordConfirm;
		if (passwordNew!=passwordConfirm) return res.json([{msg:'passwords-dont-match'}]);
		//try to find reset process
    	Reset.findOne({ email:email, ticket:ticket }).exec(function (err, reset) {
			if (!reset) return res.json([{msg:'process-expired'}]);
			//update password
			var hash=bcrypt.hashSync(passwordNew,HASH_SALT_ROUNDS);
			User.findOneAndUpdate({email:email}, {$set: { password:hash}}, function(err) {
				if (err) {
					debug(err);
					res.json([{msg:'database-error'}]);
				}
				reset.delete();
				res.json({});
			})
		});
	}
]

/*
 Start register process - send mail
*/
exports.startRegisterProcess=[

	body('login').matches(consts.REGEX_LOGIN),
	body('name').matches(consts.REGEX_USERNAME),
	body('email').matches(consts.REGEX_EMAIL),
	body('lang').matches(consts.REGEX_LANGUAGE),
	body('password').matches(consts.REGEX_PASSWORD),
	body('passwordConfirm').isLength({ min: 1 }),
	body('comment').matches(consts.REGEX_COMMENT_300),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//terms and privacy accepted?

		if (!req.body.acceptTerms || !req.body.acceptPrivacy) return res.json([{msg:'validation-error'}])
		//registration allowed
		if (!config.REGISTRATION || !config.REGISTRATION.ALLOWED) return res.json([{msg:'forbidden'}]);
		//load form data
		var userData={
			email:req.body.email.toLowerCase(),
			login:req.body.login,
			name:req.body.name,
			comment:req.body.comment,
			lang:req.body.lang,
		}
		//compare and set passwords
		req.body.passwordConfirm;
		if (req.body.password!=req.body.passwordConfirm) return res.json([{msg:'passwords-dont-match'}]);
		userData.password=bcrypt.hashSync(req.body.passwordConfirm,HASH_SALT_ROUNDS);
		//check if email or login is already in use
    	User.findOne({ email:userData.email}, function (err, test1) {
			if (test1) return res.json([{msg:'email-taken'}]);
			//search login
			User.findOne( {login:userData.login}, function (err, test2) {
				if (test2) return res.json([{msg:'login-taken'}]);
				//create reset process
				var ticket=generator.generate({length: 20, numbers: true});
				var register=new Register({user:userData, ticket:ticket, created: new Date()});
				register.save(function(err) {
					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
					checkMaxUser();
					//send mail
					var url=config.URL_PROTO+path.join(config.URL_HOST,config.URL_PREFIX,'app','#',"main-register-confirm",ticket);
					var title="Register Account";
					var html="<p>You have registered an account on page "+config.URL_HOST+"</p>";
					html+="<p>Please click the following link to complete the process:</p>";
					html+="<p><a href='"+url+"'>"+url+"</a></p>";
					html+="<p>If you did not start this request yourself, please ignore this email.</p>";
					sendInfoMail(userData.email,title,html);
					return res.json({});
				})
			})
		});
	}
]

function checkMaxUser() {
	User.find( {}, function (err, userList) {
		var title="";
		var html="";
		if (userList+config.REGISTRATION.USER_HARD_WARN >=config.REGISTRATION.MAX_USERS)
			{
				title = "User warning - severe";
				html="<p>The set user limit will very soon be reached on "+config.URL_HOST+"</p>";
				sendInfoMail(config.USER_ADMIN_MAIL,title,html);
				return false;
			}
			else if(userList+config.REGISTRATION.USER_SOFT_WARN >=config.REGISTRATION.MAX_USERS) {
				title = "User warning - notificiation";
				html="<p>The set user limit will soon be reached on "+config.URL_HOST+"</p>";
				sendInfoMail(config.USER_ADMIN_MAIL,title,html);
				return false;
			}
			else{
				debug("MAX_USERS checked");
				return true;
			}

	});
}


exports.completeRegisterProcess=[
	body('ticket').isAlphanumeric().withMessage('invalid-ticket'),
	(req,res,next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		Register.findOne({ ticket:req.body.ticket}, function (err, register) {
			if (!register) return res.json([{msg:'process-expired'}]);
			//max number exceeded?
			User.find( {}, function (err, userList) {
				if (userList && userList.length>=config.REGISTRATION.MAX_USERS) return res.json([{msg:'max-user-number-reached'}]);

				//create user
				var userData={
					name:register.user.name,
					login:register.user.login,
					password:register.user.password,
					lang:register.user.lang,
					email:register.user.email,
					comment:register.user.comment,
					roles:config.REGISTRATION.DEFAULT_ROLES,
					active:true
				}
				var user=new User(userData);
				user.save(function(err) {
					if (err) {
						debug(err);
						return res.json([{msg:'database-error'}]);
					}
					register.delete();
					checkMaxUser();
					//send mail to admin
					var title="Register Account";
					var html="<p>A new user has registered on "+config.URL_HOST+"</p>";
					html+="<p>Name: "+escapeHTML(userData.name)+"</p>";
					html+="<p>Email: "+escapeHTML(userData.email)+"</p>";
					html+="<p>Comment: "+escapeHTML(userData.comment)+"</p>";
					sendInfoMail(config.USER_ADMIN_MAIL,title,html);
					return res.json({});
				})
			})
		})
	}
]

/*
Cleaning up every 5 minutes:
 - register and reset processes
 - BruteForceFilter
 - RefreshToken
 */
cron.schedule("*/5 * * * *", cleanProcesses);
function cleanProcesses() {
	debug('Cleaning old processes and data (Reset, Captcha, Register, BruteForceFilter, RefreshToken');
	var pastDate=new Date();
	pastDate.setTime(pastDate.getTime() - 600*1000);	//10 Minutes in the past
	Captcha.deleteMany({'created': {$lte: pastDate}}, function(err) {
		if (err) debug(err);
	})
	Reset.deleteMany({'created': {$lte: pastDate}}, function(err) {
		if (err) debug(err);
	})
	Register.deleteMany({'created': {$lte: pastDate}}, function(err) {
		if (err) debug(err);
	})
	//Clean all BruteForceFilter Entries if timestamp passed
	BruteForceFilter.deleteMany({'blockedUntilTs': {$lte: Date.now()}}, function(err) {
		if (err) debug(err);
	})
	//Clean old refreshTokens
	RefreshToken.deleteMany({'expires': {$lte: Date.now()}}, function(err) {
		if (err) debug(err);
	})
	//Clean old tans
	Tan.deleteMany({'expires': {$lte: Date.now()}}, function(err) {
		if (err) debug(err);
	})
}

/***********************************************************
	Functions used from other controllers in modules
***********************************************************/

/*
 Parse Authentication header
 */
exports.parseAuthorizationHeader=function(req, res, next) {
	//Don't parse if request is directed to login or refreshToken url
	if (req.path=="/main/login" || req.path=="/main/token" || req.path=="/main/logout") return next();
	//No need for authentication on app assets
	if (req.path.indexOf('/app/')==0) return next();
	//Parse Authentication header
	var accessToken;
	var header=req.headers.authorization;
	if (header && header.startsWith("Bearer ")) accessToken=header.substring(7);
	if (!accessToken) return next();
	//Verfiy token (use secret and ip if configured)
	var ip=config.JWT.RESTRICT_IP ? requestIp.getClientIp(req) : '';
	jwt.verify(accessToken, ip+config.JWT.SECRET, function(err, payload) {
		//Token invalid - return 401, so client can request a refresh
		if (err) {
			debug('AccessToken expired: '+accessToken);
			return res.status(401).json([{msg:'token-expired'}]);
		}
		//Token valid - load user from db and add to res.locals for subsequent middleware-functions
		User.findOne({ login: payload.login }).exec(function (err, user) {
			if (err) {	//this could only happen, if user has been removed by admin
				debug(err);
				return res.json([{msg:'account-locked'}]);
			}
			if (user) res.locals.user=user;
			debug('AccessToken accepted for '+payload.login)
			next();
		})
	})
}

/*
 Check if user is authenticated and owns defined role
 */
exports.checkAuthAndRole=function(req, res, next, role) {
	//Is the user authenticated?
	if (!res.locals.user) {
		//Send 401 back, so client can try to refresh the accessToken
		return res.status(401).json([{msg:'please-login'}]);
	}
	//Is the user authorized?
	else if (!(res.locals.user.roles.indexOf(role)>-1)) {
		return res.status(403).json([{msg:'not-allowed-text'}]);
	}
	next();
}

/*
 Check if user with role admin is authenticated
 */
 exports.checkAdmin=function(req, res, next) {
	 exports.checkAuthAndRole(req,res,next,'admin');
 }

 /*
  Check if user is valid user
  */
 exports.checkIsUser=function(req,res,next) {
	 if (!res.locals.user) {
		 return res.status(401).json([{msg:'please-login'}]);
	 }
	 next();
 }


 /***********************************************************
 	Shortcuts
 ***********************************************************/

//Create shortcut for link
exports.shortenLink=[
	body('url').isURL({ require_tld: false }), //ignore tld for local installtion, for example localhost, plain hostname
	async function (req, res, next) {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var url=req.body.url;
		//search in db for link
		ShortLink.findOne({url:url}, async function(err,entry) {
			if (err) debug(err);
			if (entry && entry.ticket) return res.json({ticket:entry.ticket});
			//create new entry
			var ticket='';
			while (!ticket) {
				var ticket=generator.generate({length: SHORTLINK_TICKET_LENGTH, numbers: true});
				var entry=await ShortLink.findOne({ticket:ticket});
				if (entry) ticket='';
			}
			var ts=(new Date()).getTime();
			var newEntry=new ShortLink({ticket:ticket,ts:ts,url:url});
			newEntry.save(function(err) {
				if (err) {
					debug(err);
					return '';
				}
				return res.json({ticket:ticket});
			})
		})
	}
]

//Resolve from shortcut
exports.resolveLink=[
	sanitize('ticket').trim().escape(),
	(req, res, next)=> {
		var ticket=req.params.ticket;
		ShortLink.findOne({ticket:ticket}, function(err,entry) {
			if (err) debug(err);
			if (entry) {
				var ts=(new Date()).getTime();
				entry.ts=ts;
				entry.count=count=entry.count+1;
				entry.save(function(err) {
					if (err) debug(err);
					res.json({url:entry.url});
				})
			}
			else res.json({url:''})
		})
	}
]

//Clean up unused shortcuts
cron.schedule("0 2 * * *", cleanShortLinks);
function cleanShortLinks() {
	var tsOld=(new Date()).getTime()-1000*3600*24*SHORTLINK_LIFETIME;
	ShortLink.deleteMany({ts: { $lt: tsOld }}, function(err) {
		if (err) debug(err);
	})
}
