FROM ubuntu:20.04

RUN apt-get -y update && \
	DEBIAN_FRONTEND=noninteractive \
	apt-get install -y nodejs npm git autoconf libcurl4 ca-certificates libgl1 libxi6 nano texlive-latex-base --no-install-recommends && \
	DEBIAN_FRONTEND=noninteractive \
	apt-get -y upgrade && \
	rm -rf /var/lib/apt/lists/*

RUN groupadd -r user && useradd -d /srv -r -g user user

RUN chown user:user /srv

USER user

WORKDIR /srv

RUN git clone https://codeberg.org/lerntools/base.git

RUN mkdir -p /srv/base/modules && cd /srv/base/modules && \
	git clone https://codeberg.org/lerntools/abcd.git && \
	git clone https://codeberg.org/lerntools/about.git && \
	git clone https://codeberg.org/lerntools/ideas.git && \
	git clone https://codeberg.org/lerntools/projector.git && \
	git clone https://codeberg.org/lerntools/survey.git && \
	git clone https://codeberg.org/lerntools/tex.git

WORKDIR /srv/base

RUN npm update

RUN npm install

RUN mkdir upload data

RUN cp server.config.example.json server.config.production.json && \
	sed -i 's/localhost:27017/mongodb:27017/' server.config.production.json && \
	sed -i 's/"TAN_IGNORE": false/"TAN_IGNORE": true/' server.config.production.json 

RUN cp app.config.example.js app.config.js

RUN npm run build

CMD npm start


